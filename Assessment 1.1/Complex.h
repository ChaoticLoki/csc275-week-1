#pragma once
#include <iostream>
using namespace std;

class Complex
{
public:
	Complex(void);
	~Complex(void);
	int getReal();
	int getImaginary();
private:
	int real;
	int imaginary;
	friend ostream& operator<<(ostream&, const Complex &C);
	friend istream& operator>>(istream&, Complex &C);
	bool is_number(const string&);
};

