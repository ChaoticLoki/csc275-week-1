
#include "Complex.h";
#include <iostream>;
using namespace std;

int main()
{

	Complex *complex = new Complex();
	cout << "Your Input [x + yi]: ";
	cin >> *complex;
	cout << "Real: " << complex->getReal() << " Imaginary: " << complex->getImaginary() << endl;
	cout << "Complex: " << *complex;

	int endOfProgram;
	cin >> endOfProgram;	
}