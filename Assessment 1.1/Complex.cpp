#include "Complex.h"
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
using namespace std;

bool is_number(const string& s)
{
	string::const_iterator it = s.begin();
    while (it != s.end() && isdigit(*it)) ++it;
    return !s.empty() && it == s.end();
}

Complex::Complex()
{
	real = 0;
	imaginary = 0;
}


Complex::~Complex(void)
{
}

//Allow Read access to variable
int Complex::getReal()
{
	return this->real;
}

//Allow Read access to variable
int Complex::getImaginary()
{
	return this->imaginary;
}

ostream &operator<<(ostream &output, const Complex &C)
{
	//Default Symbol if not negative
	char symbol = '+';

	//If negative replace symbol with negative symbol
	if (C.imaginary < 0)
		symbol = '-';

	//Piece together real and imaginary numbers in proper format. make imaginary absolute because the sign is handled via the symbol variable.
	output << C.real << ' ' << symbol << ' ' << abs(C.imaginary) << 'i';
	return output;
}

istream &operator>>(istream &input, Complex &C)
{
	//Create temporary Variables
	string tempReal;
	char symbol = '+';
	string tempImaginaryString;
	int tempImaginary = 0;
	//Get User Input
	input >> tempReal >> symbol >> tempImaginaryString;
	//Get the last character of imaginary input for use later
	char lastCharacter = tempImaginaryString[tempImaginaryString.length() - 1];
	//Strip the i from 
	tempImaginaryString = tempImaginaryString.substr(0, tempImaginaryString.size()-1);
	//Check if the real input is a number
	if (!is_number(tempReal))
	{
		cin.setstate(std::ios::failbit);
	}
	//Check if stripped imaginary input is number.
	else if(!is_number(tempImaginaryString))
	{
		cin.setstate(std::ios::failbit);
	}
	//make sure symbols are either + or - only
	else if (symbol != '+' && symbol != '-')
	{
		cin.setstate(std::ios::failbit);
	}
	//Make sure last character in imaginary input is i.
	else if (lastCharacter != 'i')
	{
		cin.setstate(std::ios::failbit);
	}
	//Convert input string to integer
	tempImaginary = atoi(tempImaginaryString.c_str());
	//assign it to complex Class
	C.real = atoi(tempReal.c_str());
	C.imaginary = tempImaginary;
	//If the symbol prior to imaginary input is -, then multiply the imaginary integer by -1 to make it negative
	if( symbol == '-')
		C.imaginary *= -1;

	return input;
}

