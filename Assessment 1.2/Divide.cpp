#include "defaults.h";

float divide(int numerator, int denominator)
{
	//If the denominator is 0 throw a DivideByZeroException which is implemented in Divide.h
	if (denominator == 0)
	{
		throw DivideByZeroException();
	}
	return ((float)numerator/(float)denominator);
}

void divideQuestion()
{
	//Divide by zero exception test
	int divideInputNumerator;
	int divideInputDenominator;
	std::cout << " --== Divide by Zero ==--" << std::endl;
	std::cout << "Numerator = ";
	std::cin >> divideInputNumerator;
	std::cout << "Denominator: ";
	std::cin >> divideInputDenominator;
	try
	{
		//Try to use the divide function that handles exception throwing, if denominator is not 0 result will be printed
		float result = divide(divideInputNumerator, divideInputDenominator);
		std::cout << result << std::endl;
	}
	catch(DivideByZeroException e)
	{
		std::cout << e.what() << std::endl;
	}
}