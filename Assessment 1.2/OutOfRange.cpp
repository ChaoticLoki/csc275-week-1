#include "defaults.h";


void OutOfRangeQuestion()
{
	//Out of Range exception test
	int listOfNumbers[10] = { 1, 3, 4, 6, 7, 21, 52, 687, 2, 20 };
	int userInput;
	std::cout << " --== Out of range ==--" << std::endl;
	std::cout << "Array of 10 integers { 1, 3, 4, 6, 7, 21, 52, 687, 2, 20 }" << std::endl;
	std::cout << "Select Index: ";
	std::cin >> userInput;
	try
	{
		//If user input is outside of array range throw an out of range exception
		if (userInput < 0 || userInput > 9)
		{
			throw std::out_of_range("Exception: Index out of range!");
		}
		std::cout << listOfNumbers[userInput] << std::endl;
	}
	catch(std::out_of_range e)
	{
		std::cout << e.what() << std::endl;
	}
}
