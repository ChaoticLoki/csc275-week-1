#pragma once

struct NegativeRootException : public std::exception
{
	const char* what() const throw()
	{
		return "Exception: Can't find square root of negative numbers!";
	}
};
void SquareRootQuestion();