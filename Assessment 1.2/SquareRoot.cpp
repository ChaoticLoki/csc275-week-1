#include "defaults.h";

void SquareRootQuestion()
{
	
	try
	{
		double userInput;

		std::cout << "Square root of: ";
		std::cin >> userInput;
		//If user input is less than zero, throw a NegativeRootException implemented in SquareRoot.h
		if(userInput < 0)
			throw NegativeRootException();
		//Input is not negative, print square root of number
		std::cout << "Square root is: " << sqrt(userInput) << std::endl;
	}
	catch(NegativeRootException e)
	{
		std::cout << e.what() << std::endl;
	}
}