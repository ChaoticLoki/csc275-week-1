#include "defaults.h"


void MissingFileQuestion()
{
	std::string filename;
	try
	{
		std::string line;
		std::cout << " --== Missing File Checker ==--" << std::endl;
		std::cout << "File Name: ";
		//Get filename
		std::cin >> filename;
		std::ifstream file (filename);
		//If the file is not available, throw a FileNotOpenedException that is implemented in MissingFile.h
		if (!file.is_open())
			throw FileNotFoundException();
		//If file exists, print content
		std::cout << " --== File Contents ==--" << std::endl;
		while (getline(file, line))
		{
			std::cout << line << std::endl;
		}
	}catch(const FileNotFoundException e)
	{
		//File does not exist, generate one
		std::cout << e.what() << std::endl;	 
		std::cout << "Generating File..." << std::endl;
		std::ofstream outFile(filename);
		outFile << "The quick brown fox jumps over the lazy dog" << std::endl;
		outFile.close();
		std::cout << "Run 4 again and type the same filename to get contents" << std::endl; 
	}
}