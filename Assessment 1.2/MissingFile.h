#pragma once

struct FileNotFoundException : public std::exception
{
	const char* what() const throw()
	{
		return "Exception: File not Opened";
	}
};

void MissingFileQuestion();
