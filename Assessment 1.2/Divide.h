#pragma once
#include "defaults.h";

struct DivideByZeroException : public std::exception
{
	const char* what() const throw()
	{
		return "Exception: Cannot Divide by Zero!";
	}
};

float divide(int,int);
void divideQuestion();