#include "defaults.h";
using namespace std;



int main()
{
	int userInput;

	do{
		//Menu Handling
		cout << "==========================" << endl;
		cout << "          Menu" << endl;
		cout << "==========================" << endl;
		cout << " 1: Divide By Zero" << endl;
		cout << " 2: Out of Range" << endl;
		cout << " 3: Negative Square Roots" << endl;
		cout << " 4: Missing File" << endl;
		cout << " 0: Exit" << endl;
		cout << "==========================" << endl << endl;
		cout << "Input: ";
		cin >> userInput;
		cout << endl;
		switch (userInput)
		{
		case 1:
			divideQuestion();
			break;
		case 2:
			OutOfRangeQuestion();
			break;
		case 3:
			SquareRootQuestion();
			break;
		case 4:
			MissingFileQuestion();
			break;
		default:
			break;
		}
		cout << endl;
	}while(userInput != 0);
	
}